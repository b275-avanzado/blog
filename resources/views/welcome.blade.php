{{-- Activity for s02 --}}

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" alt="laravel logo" class="offset-3 col-6">
        </div>
    </div>

	<h2 class="mt-5 text-center">Featured Posts:</h2>

    @if(count($posts) > 0)
		@foreach($posts as $post)
			<div class="card text-center my-3">
				<div class="card-body">
					<h4 class="card-title mb-3">
						<a href="/posts/{{$post->id}}" class="text-decoration-none">
							{{$post->title}}
						</a>
					</h4>
					{{-- eloquent hava a specific functionality that automatically joins table, once the relationship is identified in the Models. --}}
					<h6 class="card-text mb-3">
						Author: {{$post->user->name}}
					</h6>
					<p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
				</div>
			</div>
		@endforeach
	@else
		<div>
			<h2>There are no post to show.</h2>
			<a href="/posts/create" class="btn btn-info">Create post</a>
		</div>
	@endif
    

@endsection