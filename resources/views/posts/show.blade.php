@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h2 class="card-title">{{$post->title}}</h2>
            <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
            <p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
            <p class="card-text">{{$post->content}}</p>

            @if(Auth::user())
                @if(Auth::id() != $post->user_id)
                    <form method="POST" action="/posts/{{$post->id}}/like" class="d-inline">
                        @method('PUT')
                        @csrf
                        @if($post->likes->contains("user_id", Auth::id()))
                            <button type="submit" class="btn btn-danger">Unlike</button>
                        @else
                        <button type="submit" class="btn btn-success">Like</button>
                        @endif
                    </form>
                @endif

                <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">Comment</button>
            @endif

            <div class="mt-3">
                <a href="/posts" class="card-link">View All Posts</a>
            </div>
        </div>
    </div>

    {{-- Comment Section --}}
        <h3 class="mt-4">Comments:</h3>
        @foreach ($comments as $comment)
            
            <div class="card my-3">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-center">{{$comment->content}}</h4>
                    <div class="float-end">
                        <p class="card-subtite mb-3">Posted by: {{$comment->user->name}}</p>
                        <p class="card-text mb-3 text-muted">Posted on: {{$comment->created_at}}</p>
                    </div>
                </div>
            </div>
        
        @endforeach
    
    
    {{-- Modal --}}
    <div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="commentModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="/posts/{{$post->id}}/comment">
                        @csrf
                        <div class="mb-3">
                        <label for="commentContent" class="col-form-label">Comment:</label>
                        <textarea class="form-control" id="commentContent" name="commentContent"></textarea>
                        </div>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Post Comment</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    
    
@endsection