<?php

use Illuminate\Support\Facades\Route;
// link the PostController file
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// define a route wherein a view(form) to create a post 
Route::get('/posts/create', [PostController::class, 'create']);

//  define a route wherein form data will be sent via POST method to the /posts URI endpoint
Route::post('/posts', [PostController::class, 'store']);

// define a route that will return a view containing all the posts
Route::get('/posts', [PostController::class, 'index']);

// define a route that will return a view for the welcome page
Route::get('/', [PostController::class, 'welcome']);

// define a route that will return a view containing only the authenticated user's posts.
Route::get('/myPosts', [PostController::class, 'myPosts']);

// define a route wherein a view showing a specific posts with matching URL parameter ID ("{}") will be returned to the user
Route::get('/posts/{id}', [PostController::class, 'show']);

// define a route that will return a view containing an edit form for a specific post
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

// define a route that will overwrite an existing post with matching URL parameter ID via PUT method
Route::put('/posts/{id}', [PostController::class, 'update']);

// define a route that will delete a post of the matching URL parameter
// Route::delete('/posts/{id}', [PostController::class, 'destroy']);

// define a route that will archive a post of the matching URL parameter
Route::delete('/posts/{id}', [PostController::class, 'archive']);

// define a route that will unarchive a post of the matching URL parameter
Route::patch('/posts/{id}', [PostController::class, 'unarchive']);

// define a route that will call the "like action" when a PUT request is received at the '/posts/{id}/like endpoint
Route::put('/posts/{id}/like', [PostController::class, 'likes']);

// define a route that will create a comment on a post when a POST request is received at the '/posts/{id}/comment' endpoint
Route::post('/posts/{id}/comment', [PostController::class, 'comment']);